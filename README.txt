_________________________________________________________________________________
Assignment 1 - Nishant Gupta(13447), Ayush Agarwal(13180), Himanshu Shukla(13***)
=================================================================================

1. syscall_GetReg :
- We first read the number stored in register 4(register which is to be read), and store
  it in temp.
- Then we fetch the value stored in register temp.
- Then we write that value to register 2.
- Then Program counter of current thread is advanced.

2. syscall_GetPA :
-------------------------------------------------------------------------
+ I don't know what you have done. Also, I think it maybe wrong - @Ayush.
-------------------------------------------------------------------------

3-4. syscall_GetPID & syscall_GetPPID :
- We declare a global variable prevPid initialized to 2.
- if prevPid = 2 when a thread is being created, it is the first thread to be created and
  we set its  ppid = 1.
- Everytime a thread is created, we set its pid = prevPid and prevPid is incremented by 1.
- Since these variables are private we have written two functions(retPid() and retPPid())
  to get their value.
- Then Program counter of current thread is advanced.

5. syscall_NumInstr : 
- we keep a variable numInstr for each thread initialized to 0 when a thread is created.
- we increment the variable numInstr for currentThread whenever OneInstruction() is
  called because it executes one instruction.
- we write the value of numInstr of currentThread to register 2.
- Then Program counter of current thread is advanced.

6. syscall_Time :
- stats->totalTicks stores total number of ticks from the time machine started.
- we write the value of stats->totalTicks() to register 2.
- Then Program counter of current thread is advanced.

7. syscall_Yield :
- There is a YieldCPU() function in NachOSThread Class that does exactly this.
- we first advance program counters.
- Then we call currentThread->YieldCPU().

8. syscall_Sleep :
- In syscall_Sleep also we first increase program counters.
- We create a new list called sleepList in Scheduler class. this list keeps track of all
  the threads that are currently sleeping. We maintain this list sorted according to
  wakeup time of processes by always calling SortedInsert on list.
- We have also written two functions in Scheduler class -
     putToSleep(sleepTime) - It takes the sleepTime and inserts currentThread into
     sleepList with (sleepTime + currentTime) as key. Then it turns off  interrupts and
     puts currentThread to sleep. Then it turns on the interrupts.
     wakeFromSleep() - traverses sleepList from left to right and calls ReadyToRun() on
     each thread till key < currentTime.
- we have modified TimerInterruptHandler() so that  wakeFromSleep() is called on each
  timer interrupt. It wakes up all the processes whose time to wakeup is less than
  currentTime.
- Then if we read sleepTime from register 4, if sleepTime is 0 then we just do
  currentThread->YieldCPU().  else currentThread->putToSleep(sleepTime) is called.

9. syscall_Exec :
- We read the pointer to filename from register 4 and then read filename using a for loop.
- We keep a global variable filledPages which is a count of the current number of physical
  pages filled.  In the addrSpace constructor, it is incremented by numPages only at the
  end of the call.
- If the supplied executable is Null, we just return back to original program and continue executing.
- Otherwise a new addrSpace is created, and attached to the thread and old executable is deleted.
- After that we initialize the registers, restore them and just run the thread


10. syscall_Exit :
- we maintain a global variable processCount that stores the number of processes currently
  active in system. We decrease it by 1 whenever NachOSThread::FinishThread() is called.
  and increase it by 1 whenever Machine::Run() is called.
- (This is needed for join)we keep a waitingThread(which is a pointer to a NachOSThread)
  in each process. It stores a pointer to its parentThread if its parent is waiting for it
  to exit, else it is NULL. we also keep a pointer to parentThread in each thread.
- we read the exit status of program and check currentThread->waitingThread. If it is NULL
  then we store it in scheduler->exitList along with PID. If not we save the return value
  of process for future reference(in scheduler->pidList and schedule->retList)
- then we set the ppid of all child processes to 0(using Mapcar on childList).
- If number of processes is more than 1 we simply call currentThread->FinishThread()
  else we call Cleanup();

11. syscall_Fork : 
- increment the program counter.
- we create a child thread and attach a new addrSpace to it. A new AddrSpace constructor
  is defined which just allocates pageTable to the child thread.
- we copy the contents of memory of parent to child's memory in Main Memory  while
  maintaining the appropriate offset.
- Then we save the user state in child. Thus all the registers are saved along with the
  next instruction. Finally set the second register's value in child to be 0.
- Then we allocate stack to the child and put it in the ready queue using
  machine->ReadyToRun().


12. syscall_Join : 
- we keep a childList for each Thread with its pid as key and thread pointer as item.
- we have implemented a search function for lists. it takes a key and returns the pointer
  to the element matching the key, returns NULL if not found.
- we keep a waitingThread(which is a pointer to a NachOSThread) in each process. It stores
  a pointer to its parentThread if its parent is waiting for it to exit, else it is NULL.
- we search currentThread->childList to see if any child has that PID, If not, return -1.
- If the child was found, then we search for child with that PID in scheduler->pidList.
  If found, we return the exit status of child (stored in scheduler->retList).
- If not found, then we store currentThread in childThread->waitingThread. and put
  currentThread to sleep.
