You don't need to ssh manually to do any work.

Change USER to your cse username in SampleMakefile and Samplerun. Then rename them to Makefile and run respectively.

do ```make copy``` to sync your files with server.

do ```make test``` to re-compile tests.

do ```make source``` to recompile source.

Or simply ```make``` to do all three.

use ```./run testName``` to run test . eg. ```./run printtest```.

Also I suggest to not modify any of supplied tests. Create new tests from them. Also modify nachos/code/test/Makefile to compile your new tests also.