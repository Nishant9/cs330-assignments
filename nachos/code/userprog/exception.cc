// exception.cc 
//	Entry point into the Nachos kernel from user programs.
//	There are two kinds of things that can cause control to
//	transfer back to here from user code:
//
//	syscall -- The user code explicitly requests to call a procedure
//	in the Nachos kernel.  Right now, the only function we support is
//	"Halt".
//
//	exceptions -- The user code does something that the CPU can't handle.
//	For instance, accessing memory that doesn't exist, arithmetic errors,
//	etc.  
//
//	Interrupts (which can also cause control to transfer from user
//	code into the Nachos kernel) are handled elsewhere.
//
// For now, this only handles the Halt() system call.
// Everything else core dumps.
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#include "copyright.h"
#include "system.h"
#include "syscall.h"
#include "console.h"
#include "synch.h"
#include "machine.h"
#include "addrspace.h"
//----------------------------------------------------------------------
// ExceptionHandler
// 	Entry point into the Nachos kernel.  Called when a user program
//	is executing, and either does a syscall, or generates an addressing
//	or arithmetic exception.
//
// 	For system calls, the following is the calling convention:
//
// 	system call code -- r2
//		arg1 -- r4
//		arg2 -- r5
//		arg3 -- r6
//		arg4 -- r7
//
//	The result of the system call, if any, must be put back into r2. 
//
// And don't forget to increment the pc before returning. (Or else you'll
// loop making the same system call forever!
//
//	"which" is the kind of exception.  The list of possible exceptions 
//	are in machine.h.
//----------------------------------------------------------------------
static Semaphore *readAvail;
static Semaphore *writeDone;
static void ReadAvail(int arg) { readAvail->V(); }
static void WriteDone(int arg) { writeDone->V(); }

void func(int temp){
  if (threadToBeDestroyed != NULL) {
    delete threadToBeDestroyed;
    threadToBeDestroyed = NULL;
    //        (machine->processCount)--;
  }

  if (currentThread->space != NULL) {		// if there is an address space
    currentThread->RestoreUserState();     // to restore, do it.
    currentThread->space->RestoreState();
  }
  machine->Run();
}

/**/
void zeropid(int a) {
if((NachOSThread*)a != NULL) {
  ((NachOSThread *)a)->SetPPid(0);
  ((NachOSThread *)a)->parentThread = NULL;
 }
}
/**/

static void ConvertIntToHex (unsigned v, Console *console)
{
  unsigned x;
  if (v == 0) return;
  ConvertIntToHex (v/16, console);
  x = v % 16;
  if (x < 10) {
    writeDone->P() ;
    console->PutChar('0'+x);
  }
  else {
    writeDone->P() ;
    console->PutChar('a'+x-10);
  }
}

void
ExceptionHandler(ExceptionType which)
{
  int type = machine->ReadRegister(2);
  int memval, vaddr, printval, tempval, exp, id, found;
  unsigned printvalus;        // Used for printing in hex
  if (!initializedConsoleSemaphores) {
    readAvail = new Semaphore("read avail", 0);
    writeDone = new Semaphore("write done", 1);
    initializedConsoleSemaphores = true;
  }
  Console *console = new Console(NULL, NULL, ReadAvail, WriteDone, 0);;

  if ((which == SyscallException) && (type == syscall_Halt)) {
    DEBUG('a', "Shutdown, initiated by user program.\n");
    interrupt->Halt();
  }
  else if ((which == SyscallException) && (type == syscall_PrintInt)) {
    printval = machine->ReadRegister(4);
    if (printval == 0) {
      writeDone->P() ;
      console->PutChar('0');
    }
    else {
      if (printval < 0) {
        writeDone->P() ;
        console->PutChar('-');
        printval = -printval;
      }
      tempval = printval;
      exp=1;
      while (tempval != 0) {
        tempval = tempval/10;
        exp = exp*10;
      }
      exp = exp/10;
      while (exp > 0) {
        writeDone->P() ;
        console->PutChar('0'+(printval/exp));
        printval = printval % exp;
        exp = exp/10;
      }
    }
    // Advance program counters.
    machine->WriteRegister(PrevPCReg, machine->ReadRegister(PCReg));
    machine->WriteRegister(PCReg, machine->ReadRegister(NextPCReg));
    machine->WriteRegister(NextPCReg, machine->ReadRegister(NextPCReg)+4);
  }
  else if ((which == SyscallException) && (type == syscall_PrintChar)) {
    writeDone->P() ;
    console->PutChar(machine->ReadRegister(4));   // echo it!
    // Advance program counters.
    machine->WriteRegister(PrevPCReg, machine->ReadRegister(PCReg));
    machine->WriteRegister(PCReg, machine->ReadRegister(NextPCReg));
    machine->WriteRegister(NextPCReg, machine->ReadRegister(NextPCReg)+4);
  }
  else if ((which == SyscallException) && (type == syscall_PrintString)) {
    vaddr = machine->ReadRegister(4);
    machine->ReadMem(vaddr, 1, &memval);
    while ((*(char*)&memval) != '\0') {
      writeDone->P() ;
      console->PutChar(*(char*)&memval);
      vaddr++;
      machine->ReadMem(vaddr, 1, &memval);
    }
    // Advance program counters.
    machine->WriteRegister(PrevPCReg, machine->ReadRegister(PCReg));
    machine->WriteRegister(PCReg, machine->ReadRegister(NextPCReg));
    machine->WriteRegister(NextPCReg, machine->ReadRegister(NextPCReg)+4);
  }
  else if ((which == SyscallException) && (type == syscall_PrintIntHex)) {
    printvalus = (unsigned)machine->ReadRegister(4);
    writeDone->P() ;
    console->PutChar('0');
    writeDone->P() ;
    console->PutChar('x');
    if (printvalus == 0) {
      writeDone->P() ;
      console->PutChar('0');
    }
    else {
      ConvertIntToHex (printvalus, console);
    }
    // Advance program counters.
    machine->WriteRegister(PrevPCReg, machine->ReadRegister(PCReg));
    machine->WriteRegister(PCReg, machine->ReadRegister(NextPCReg));
    machine->WriteRegister(NextPCReg, machine->ReadRegister(NextPCReg)+4);
  }
  else if ((which == SyscallException) && (type == syscall_GetReg)) {
    int temp = machine->ReadRegister(4);
    if(temp >= 0 && temp < NumTotalRegs){
      machine->WriteRegister(2,machine->ReadRegister(temp));
    }
    else{
      printf("Register Out of bounds: Unexpected User mode Exception %d %d\n", which, type);
      ASSERT(FALSE);
    }
    machine->WriteRegister(PrevPCReg, machine->ReadRegister(PCReg));
    machine->WriteRegister(PCReg, machine->ReadRegister(NextPCReg));
    machine->WriteRegister(NextPCReg, machine->ReadRegister(NextPCReg)+4);
  }
  else if ((which == SyscallException) && (type == syscall_GetPA)) {
    unsigned int vadd = machine->ReadRegister(4);
    unsigned int vpn = (unsigned) vadd / PageSize;
    unsigned int offset = (unsigned) vadd % PageSize;
    int physAddr;
    TranslationEntry *entry;
    if(vpn >= machine->pageTableSize) {
      machine->WriteRegister(2,-1);
    }
    else {
      if(!machine->pageTable[vpn].valid){
        machine->WriteRegister(2,-1);
      }
      else {
        entry = &(currentThread->space->pageTable[vpn]);
        int pageFrame = entry->physicalPage;
        if(pageFrame >= NumPhysPages)
          machine->WriteRegister(2,-1);
        else {
          physAddr = pageFrame * PageSize + offset;
          machine->WriteRegister(2,physAddr);
        }
      }
    }
    machine->WriteRegister(PrevPCReg, machine->ReadRegister(PCReg));
    machine->WriteRegister(PCReg, machine->ReadRegister(NextPCReg));
    machine->WriteRegister(NextPCReg, machine->ReadRegister(NextPCReg)+4);
  }
  else if ((which == SyscallException) && (type == syscall_GetPID)) {
    id = currentThread->retPid();
    machine->WriteRegister(2, id);
    machine->WriteRegister(PrevPCReg, machine->ReadRegister(PCReg));
    machine->WriteRegister(PCReg, machine->ReadRegister(NextPCReg));
    machine->WriteRegister(NextPCReg, machine->ReadRegister(NextPCReg)+4);
  }
  else if ((which == SyscallException) && (type == syscall_GetPPID)) {
    id = currentThread->retPPid();
    machine->WriteRegister(2, id);
    machine->WriteRegister(PrevPCReg, machine->ReadRegister(PCReg));
    machine->WriteRegister(PCReg, machine->ReadRegister(NextPCReg));
    machine->WriteRegister(NextPCReg, machine->ReadRegister(NextPCReg)+4);
  }
  else if ((which == SyscallException) && (type == syscall_Time)) {
    printvalus = stats->totalTicks;
    machine->WriteRegister(2,printvalus);
    machine->WriteRegister(PrevPCReg, machine->ReadRegister(PCReg));
    machine->WriteRegister(PCReg, machine->ReadRegister(NextPCReg));
    machine->WriteRegister(NextPCReg, machine->ReadRegister(NextPCReg)+4);
  }
  else if ((which == SyscallException) && (type == syscall_NumInstr)) {
    printvalus = currentThread->numInstr;
    machine->WriteRegister(2,printvalus);
    machine->WriteRegister(PrevPCReg, machine->ReadRegister(PCReg));
    machine->WriteRegister(PCReg, machine->ReadRegister(NextPCReg));
    machine->WriteRegister(NextPCReg, machine->ReadRegister(NextPCReg)+4);
  }
  else if ((which == SyscallException) && (type == syscall_Yield)) {
    machine->WriteRegister(PrevPCReg, machine->ReadRegister(PCReg));
    machine->WriteRegister(PCReg, machine->ReadRegister(NextPCReg));
    machine->WriteRegister(NextPCReg, machine->ReadRegister(NextPCReg)+4);
    currentThread->YieldCPU();
  }
  else if ((which == SyscallException) && (type == syscall_Exec)) {
    vaddr = machine->ReadRegister(4);
    char filename[100];
    int i=0;
    machine->ReadMem(vaddr, 1, &memval);
    while ((*(char*)&memval) != '\0') {
      filename[i++]=memval;
      //console->PutChar(*(char*)&memval);
      vaddr++;
      machine->ReadMem(vaddr, 1, &memval);
    }
    filename[i]='\0';
    OpenFile *executable = fileSystem->Open(filename);
    AddrSpace *space;

    if (executable == NULL) {
      printf("returning from exec");
        machine->WriteRegister(PrevPCReg, machine->ReadRegister(PCReg));
        machine->WriteRegister(PCReg, machine->ReadRegister(NextPCReg));
        machine->WriteRegister(NextPCReg, machine->ReadRegister(NextPCReg)+4);
    } else{
        space = new AddrSpace(executable);
        currentThread->space = space;

        delete executable;      // close file

        space->InitRegisters();   // set the initial register values
        space->RestoreState();    // load page table register
        processCount--;
        machine->Run();     // jump to the user progam
        ASSERT(FALSE);      // machine->Run never returns;
        // the address space exits
        // by doing the syscall "exit"
    }

  }
  else if ((which == SyscallException) && (type == syscall_Sleep)) {
    machine->WriteRegister(PrevPCReg, machine->ReadRegister(PCReg));
    machine->WriteRegister(PCReg, machine->ReadRegister(NextPCReg));
    machine->WriteRegister(NextPCReg, machine->ReadRegister(NextPCReg)+4);
    int sleepTime = (unsigned)machine->ReadRegister(4);
    if(sleepTime == 0) currentThread->YieldCPU();
    else scheduler->putToSleep(sleepTime);
  }
  else if ((which == SyscallException) && (type == syscall_Fork)) {
    machine->WriteRegister(PrevPCReg, machine->ReadRegister(PCReg));
    machine->WriteRegister(PCReg, machine->ReadRegister(NextPCReg));
    machine->WriteRegister(NextPCReg, machine->ReadRegister(NextPCReg)+4);
    NachOSThread *child = new NachOSThread("forked child");
    int numPages = currentThread->space->NumPages();
    int filledPages = currentThread->space->getFilledPages();
    AddrSpace *space = new AddrSpace(currentThread->space);
    for(int i=0; i<numPages*PageSize; i++)
      *(machine->mainMemory + (filledPages)*PageSize + i) = *(machine->mainMemory + currentThread->space->pageTable[0].physicalPage*PageSize + i);
    filledPages +=numPages;
    child->space = space;
    child->SaveUserState();
    child->SetSecondRegister(0);
    child->ThreadStackAllocate(&func, 0);
    currentThread->childList->PrependWithKey(child, child->retPid());
    IntStatus oldStatus = interrupt->SetLevel(IntOff);
    scheduler->ReadyToRun(child);
    interrupt->SetLevel(oldStatus);
    machine->WriteRegister(2,child->retPid());
  }
  else if ((which == SyscallException) && (type == syscall_Exit)) {
    int tempval = machine->ReadRegister(4);
    if(currentThread->waitingThread != NULL) {
      currentThread->waitingThread->SetSecondRegister(tempval);
      IntStatus oldLevel = interrupt->SetLevel(IntOff);
      scheduler->ReadyToRun(currentThread->waitingThread);
      (void) interrupt->SetLevel(oldLevel);
    }
    else scheduler->setRetVal(currentThread->retPid(),tempval);
    if(currentThread->parentThread != NULL)
      currentThread->parentThread->childList->SetNULL(currentThread->retPid(), &found);
    currentThread->childList->Mapcar((VoidFunctionPtr) zeropid);
    if(processCount > 1) currentThread->FinishThread();
    else Cleanup();
  }
  else if ((which == SyscallException) && (type == syscall_Join)) {
    machine->WriteRegister(PrevPCReg, machine->ReadRegister(PCReg));
    machine->WriteRegister(PCReg, machine->ReadRegister(NextPCReg));
    machine->WriteRegister(NextPCReg, machine->ReadRegister(NextPCReg)+4);
    tempval = machine->ReadRegister(4);
    NachOSThread * child = (NachOSThread*)currentThread->childList->Search(tempval, &found);
    if(found) { // This means pid is valid.
      int retval = scheduler->getRetVal(tempval, &found); //search in scheduler->pidList.
      if(found) machine->WriteRegister(2,retval); // If found, child has exited.
      else { //otherwise set waitingThread and put to sleep
        child->waitingThread = currentThread;
        IntStatus oldLevel = interrupt->SetLevel(IntOff);
        currentThread->PutThreadToSleep();
        (void) interrupt->SetLevel(oldLevel);
      }
    }
    else machine->WriteRegister(2,-1);
  }
  else {
    printf("Unexpected user mode exception %d %d\n", which, type);
    ASSERT(FALSE);
  }
}

