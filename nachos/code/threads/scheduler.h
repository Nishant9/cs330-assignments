// scheduler.h 
//	Data structures for the thread dispatcher and scheduler.
//	Primarily, the list of threads that are ready to run.
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#ifndef SCHEDULER_H
#define SCHEDULER_H

#define MaxNumProcess 40

#include "copyright.h"
#include "list.h"
#include "thread.h"

// The following class defines the scheduler/dispatcher abstraction --
// the data structures and operations needed to keep track of which 
// thread is running, and which threads are ready but not running.

class Scheduler {
  public:
    Scheduler();			// Initialize list of ready threads 
    ~Scheduler();			// De-allocate ready list

    void ReadyToRun(NachOSThread* thread);	// Thread can be dispatched.
    NachOSThread* FindNextToRun();		// Dequeue first thread on the ready 
					// list, if any, and return thread.
    void Run(NachOSThread* nextThread);	// Cause nextThread to start running
    void Print();			// Print contents of ready list
    void putToSleep(int sleepTime);
    void wakeFromSleep();
    int getRetVal(int pid, int * found);
    void setRetVal(int pid, int retval);
  private:
    int pidList[MaxNumProcess], retList[MaxNumProcess];
    List *sleepList;
    List *readyList;  		// queue of threads that are ready to run,
				// but not running
};

#endif // SCHEDULER_H
